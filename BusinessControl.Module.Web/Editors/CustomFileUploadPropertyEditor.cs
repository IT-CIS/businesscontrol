﻿using BusinessControl.BaseClesses;
using BusinessControl.DocFlow;
using BusinessControl.SystemDir;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Web;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Module.Web.Editors
{
    public interface IModelCustomFileUploadPropertyEditor : IModelViewItem { }

    [ViewItem(typeof(IModelCustomFileUploadPropertyEditor))]
    public partial class CustomFileUploadPropertyEditor : ViewItem
    {
        public CustomFileUploadPropertyEditor(IModelViewItem modelNode, Type objectType) : base(objectType, modelNode.Id) { }
        // Dennis: Creates a control for the Edit mode.
        protected override object CreateControlCore()
        {
            ASPxUploadControl result = new ASPxUploadControl();
            result.AdvancedModeSettings.EnableDragAndDrop = true;
            result.AdvancedModeSettings.EnableMultiSelect = true;
            result.AdvancedModeSettings.EnableFileList = true;

            result.ShowTextBox = true;
            result.ShowProgressPanel = true;
            result.Height = 50;
            result.Size = 50;
            result.AdvancedModeSettings.DropZoneText = "Добавить файл(ы)";
            result.ShowUploadButton = true;
            result.AutoStartUpload = false;
            //result.ShowAddRemoveButtons = true;
            //result..ClientSideEvents..CurrentFolderChanged = "function(s, e) { FileManagerCurrentFolderChanged(s, e); }";

            result.FileUploadComplete += new EventHandler<FileUploadCompleteEventArgs>(FilesUpload_Complete);
            return result;
        }
        void FilesUpload_Complete(object sender, FileUploadCompleteEventArgs e)
        {
            if (View is DetailView)
            {
                if (View.ObjectTypeInfo.Type.FullName == "BusinessControl.DocFlow.DocumentBase")
                {

                    DocumentBase document = View.CurrentObject as DocumentBase;

                    UnitOfWork unitOfWork = (UnitOfWork)document.Session;
                    Connect connect = Connect.FromUnitOfWork(unitOfWork);

                    AttachmentFiles att = connect.CreateObject<AttachmentFiles>();
                    att.Name = e.UploadedFile.FileName;
                    FileData filedata = connect.CreateObject<FileData>();
                    filedata.LoadFromStream(e.UploadedFile.FileName, e.UploadedFile.FileContent);
                    filedata.Save();
                    att.File = filedata; 
                    att.RegDate = DateTime.Now.Date;
                    att.Save();
                    document.AttachmentFiles.Add(att);
                    unitOfWork.CommitChanges();
                    //View.ObjectSpace.Refresh();
                }

            }
        }

        protected void RefreshFrame(Connect connect)
        {
            Frame frame = connect.FindFirstObject<Frame>(x => x.View == View);
            if(frame != null)
            {
                RefreshController controller = frame.GetController<RefreshController>();
                if (controller != null)
                    controller.RefreshAction.DoExecute();
            }

            //View.Refresh();
            //RefreshController controller = Frame.GetController<RefreshController>();
            //if (controller != null)
            //    controller.RefreshAction.DoExecute();
        }
    }
}

