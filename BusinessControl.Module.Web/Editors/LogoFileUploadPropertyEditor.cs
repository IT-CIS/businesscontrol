﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.Drawing;
using DevExpress.Web;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.ExpressApp.Model;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Web;
using DevExpress.Xpo;
using BusinessControl.SystemDir;
using BusinessControl.OrgStructure;
using BusinessControl.Clients;

namespace BusinessControl.Module.Web.Editors
{
    public interface IModelLogoFileUploadPropertyEditor : IModelViewItem { }

    [ViewItem(typeof(IModelLogoFileUploadPropertyEditor))]
    public partial class LogoFileUploadPropertyEditor : ViewItem
    {
        public LogoFileUploadPropertyEditor(IModelViewItem modelNode, Type objectType) : base(objectType, modelNode.Id) { }
        // Dennis: Creates a control for the Edit mode.
        protected override object CreateControlCore()
        {
            ASPxUploadControl result = new ASPxUploadControl();
            result.AdvancedModeSettings.EnableDragAndDrop = true;
            result.AdvancedModeSettings.EnableMultiSelect = false;
            result.AdvancedModeSettings.EnableFileList = false;
            
            result.ShowTextBox = true;
            result.ShowProgressPanel = false;
            result.Size = 50;
            result.AdvancedModeSettings.DropZoneText = "Перетащите лого в эту зону";
            result.ShowUploadButton = false;
            result.AutoStartUpload = true;
            //result.ShowAddRemoveButtons = true;
            //result..ClientSideEvents..CurrentFolderChanged = "function(s, e) { FileManagerCurrentFolderChanged(s, e); }";

            result.FileUploadComplete += new EventHandler<FileUploadCompleteEventArgs>(FilesUpload_Complete);
            
            
            //result.UploadedFiles
            //// добавляем подсчет услуги фотографирования контейнеров
            //try
            //{
            //    int filescount = result.FileInputCount;
            //    if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.Containers.Container")
            //    {
            //        Container container = View.CurrentObject as Container;

            //        UnitOfWork unitOfWork = (UnitOfWork)container.Session;
            //        Connect connect = Connect.FromUnitOfWork(unitOfWork);
            //        Tariff photoTariff = null;
            //        Contract contract = connect.FindFirstObject<Contract>(mc => mc.Client == container.Owner && mc.ActiveFlag == true);
            //        if (contract != null)
            //        {
                        
            //            TariffScale tariffScale = connect.FindFirstObject<TariffScale>(mc => mc.Contract == contract && mc.ActiveFlag == true);
            //            if (tariffScale != null)
            //            {
            //                //3-4//
            //                IQueryable tariffs = connect.FindObjects<Tariff>(mc => mc.TariffBase == tariffScale);
            //                foreach (Tariff tariff in tariffs)
            //                {
            //                    try
            //                    {
            //                        if (tariff?.TariffService?.ServiceType?.Name == "Фотографирование контейнера")
            //                        {
            //                            foreach (dContainerSubType containerSubType in tariff.TariffService.ContainerSubTypes)
            //                            {
            //                                if (container.ContainerType.ContainerSubType == containerSubType)
            //                                {
            //                                    photoTariff = tariff;
            //                                    break;
            //                                }
            //                            }
            //                        }
            //                    }
            //                    catch { }
            //                }
            //            }
            //        }
            //        if (photoTariff != null)
            //        { }
            //        //                ContainerPhoto photo = connect.CreateObject<ContainerPhoto>();
            //        //photo.Name = e.UploadedFile.FileName;
            //        //photo.PhotoContainer = e.UploadedFile.FileBytes;
            //        //photo.PhotoDate = DateTime.Now.Date;
            //        //photo.Container = container;
            //        //photo.Save();
            //        //unitOfWork.CommitChanges();
            //    }
            //}
            //catch { }
            return result;

            //System.Web.UI.Control userControl = WebWindow.CurrentRequestPage.LoadControl("FileUploadController.ascx");

            ////((IWebUserControl1)userControl).RootFolder = "Obj";
            //return userControl;
        }
        void FilesUpload_Complete(object sender, FileUploadCompleteEventArgs e)
        {
            if (View is DetailView)
            {
                //if (View.ObjectTypeInfo.Type..FullName == "TerminalSystem2.Containers.Container")
                if (View.ObjectTypeInfo.Type.FullName == "BusinessControl.Clients.Client")
                {

                    Client client = View.CurrentObject as Client;

                    UnitOfWork unitOfWork = (UnitOfWork)client.Session;
                    Connect connect = Connect.FromUnitOfWork(unitOfWork);

                    client.Logo = e.UploadedFile.FileBytes;

                    client.Save();
                    unitOfWork.CommitChanges();
                }
                //if (View.ObjectTypeInfo.Type.FullName == "TerminalSystem2.Containers.ContainerPhotoAct")
                //{

                //    ContainerPhotoAct сontainerPlacementAct = View.CurrentObject as ContainerPhotoAct;
                //    Container container = сontainerPlacementAct.Container;
                //    UnitOfWork unitOfWork = (UnitOfWork)сontainerPlacementAct.Session;
                //    Connect connect = Connect.FromUnitOfWork(unitOfWork);

                //    ContainerPhoto photo = connect.CreateObject<ContainerPhoto>();
                //    photo.Name = e.UploadedFile.FileName;
                //    photo.PhotoContainer = e.UploadedFile.FileBytes;
                //    photo.PhotoDate = DateTime.Now.Date;
                //    photo.Container = container;
                //    photo.Save();
                //    unitOfWork.CommitChanges();
                //}

            }
        }
    }
}
