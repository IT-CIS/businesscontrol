﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using System.Drawing;
using DevExpress.Web;

namespace BusinessControl.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class GridListViewController : ViewController
    {
        public GridListViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            DevExpress.ExpressApp.Web.SystemModule.ListViewController controller = Frame.GetController<DevExpress.ExpressApp.Web.SystemModule.ListViewController>();
            if (View.Model.AllowEdit == true)
            {
                controller.EditAction.Active.SetItemValue("EditableListView", false);
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
            ASPxGridListEditor listEditor = ((ListView)View).Editor as ASPxGridListEditor;
            if (listEditor != null)
            {
                listEditor.Grid.Styles.AlternatingRow.BackColor = Color.FromArgb(244, 244, 244);
                if (View.Model.AllowEdit == true)
                {
                    //listEditor.Grid.SettingsCommandButton.EditButton.Image.Reset();
                    listEditor.Grid.SettingsCommandButton.EditButton.RenderMode = DevExpress.Web.GridCommandButtonRenderMode.Image;
                    //listEditor.Grid.SettingsCommandButton.NewButton.Image.Reset();
                    listEditor.Grid.SettingsCommandButton.NewButton.RenderMode = DevExpress.Web.GridCommandButtonRenderMode.Image;
                    listEditor.Grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
                    listEditor.Grid.SettingsPager.Visible = false;//.Templates.StatusBar = null;

                }
            }
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
