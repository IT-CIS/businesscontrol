﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace BusinessControl.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class OpenDocumentFileController : ViewController
    {
        ActionUrl actionUrl;

        public OpenDocumentFileController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            actionUrl = new ActionUrl(this, "OpenDocument", "ListView");
            actionUrl.UrlFieldName = "Oid";
            
            actionUrl.Caption = "Открыть";
            actionUrl.ImageName = "BO_Document";
            actionUrl.PaintStyle = ActionItemPaintStyle.Image;
            actionUrl.TextFieldName = "File";
            actionUrl.TextFormatString = "Открыть";
            string r = HttpContext.Current.Request.Path;
            //string t = WebWindow.CurrentRequestPage.Master.ToString();
            actionUrl.UrlFormatString = "http://localhost:62345/OpenDocument.aspx?oid={0}";
            actionUrl.SelectionDependencyType = SelectionDependencyType.RequireSingleObject;

            //string relativeUrl = string.Format(CultureInfo.InvariantCulture, "~/OpenDocument.aspx?oid={0}", attach.Oid);
            //actionUrl.UrlFormatString = WebWindow.CurrentRequestPage.ResolveUrl(@"~/OpenDocument.aspx?oid={0}");
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            

        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

    }
}
