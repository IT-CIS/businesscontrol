﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using BusinessControl.BaseClesses;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace BusinessControl.Module.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class OpentDocumentFileController : ViewController
    {
        ActionUrl actionUrl;
        AttachmentFiles attach;

        public OpentDocumentFileController(AttachmentFiles a_attach)
        {
            InitializeComponent();

            attach = a_attach;
            actionUrl = new ActionUrl(this, "OpenDocument", "Edit");
            // Target required Views (via the TargetXXX properties) and create their Actions.
            //actionUrl = new ActionUrl(this, "OpenDocument", "Edit");
            actionUrl.SelectionDependencyType = SelectionDependencyType.RequireSingleObject;
            //actionUrl.Caption = "11";
            //actionUrl.ImageName = "BO_Document";
            //actionUrl.Executed += ActionUrl_Executed;
        }


        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

            //attach = (AttachmentFiles)View.CurrentObject;
            string relativeUrl = string.Format(CultureInfo.InvariantCulture, "~/OpenDocument.aspx?oid={0}", attach.Oid);
            actionUrl.UrlFormatString = WebWindow.CurrentRequestPage.ResolveUrl(relativeUrl);
            WebApplication.Instance.ObjectSpaceCreated += Instance_ObjectSpaceCreated;


            //if (View.SelectedObjects.Count != 0)
            //{
            //    attach = (AttachmentFiles)View.SelectedObjects[0];
            ////    attach = (AttachmentFiles)View.CurrentObject;
            //    string relativeUrl = string.Format(CultureInfo.InvariantCulture, "~/OpenDocument.aspx?oid={0}", attach.Oid);
            //    actionUrl.UrlFormatString = WebWindow.CurrentRequestPage.ResolveUrl(relativeUrl);
            //    WebApplication.Instance.ObjectSpaceCreated += Instance_ObjectSpaceCreated;
            //}

        }

        public void Test()
        {
            string relativeUrl = string.Format(CultureInfo.InvariantCulture, "~/OpenDocument.aspx?oid={0}", attach.Oid);
            actionUrl.UrlFormatString = WebWindow.CurrentRequestPage.ResolveUrl(relativeUrl);
            View.Refresh(true);
            //WebApplication.Instance.ObjectSpaceCreated += Instance_ObjectSpaceCreated;
        }

        private void Instance_ObjectSpaceCreated(object sender, ObjectSpaceCreatedEventArgs e)
        {
            e.ObjectSpace.ObjectChanged += ObjectSpace_ObjectChanged;
        }

        private void ObjectSpace_ObjectChanged(object sender, ObjectChangedEventArgs e)
        {
            if (e.Object is FileData)
            {
                var fileData = (FileData)e.Object;
                if (fileData.Oid == attach.File.Oid)
                {
                    View.Refresh(true);
                }
            }
        }

        protected override void OnDeactivated()
        {
            WebApplication.Instance.ObjectSpaceCreated -= Instance_ObjectSpaceCreated;
            base.OnDeactivated();
        }

    }
}
