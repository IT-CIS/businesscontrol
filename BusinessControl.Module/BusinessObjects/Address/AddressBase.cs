﻿using BusinessControl.BaseClesses;
using BusinessControl.Classifiers;
using BusinessControl.Enums;
using BusinessControl.Interfaces;
using BusinessControl.OrgStructure;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Address
{
    /// <summary>
    /// Юридическое лицо
    /// </summary>
    [ModelDefault("Caption", "Адрес")]
    //[NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    public class AddressBase : BaseObjectXAF, IAddress
    {
        public AddressBase(Session session) : base(session) { }

        private OrganizationBase i_OrganizationBase;
        [Association, DisplayName("Организация")]
        public OrganizationBase OrganizationBase
        {
            get { return i_OrganizationBase; }
            set { SetPropertyValue("OrganizationBase", ref i_OrganizationBase, value); }
        }

        private eAddressType i_EAddressType;
        [DisplayName("Тип адреса")]
        public eAddressType EAddressType
        {
            get { return i_EAddressType; }
            set { SetPropertyValue("EAddressType", ref i_EAddressType, value); }
        }

        private bool i_isDocFlag;
        [DisplayName("Для документов")]
        public bool isDocFlag
        {
            get { return i_isDocFlag; }
            set { SetPropertyValue("isDocFlag", ref i_isDocFlag, value); }
        }

        private string i_Index;
        [Size(6), DisplayName("Индекс")]
        public string Index
        {
            get { return i_Index; }
            set { SetPropertyValue("Index", ref i_Index, value); }
        }

        private dRegion i_Region;
        [DisplayName("Регион")]
        [ImmediatePostData]
        public dRegion Region
        {
            get { return i_Region; }
            set { SetPropertyValue("Region", ref i_Region, value); }
        }


        private dCity i_City;
        [DisplayName("Город")]
        [DataSourceCriteria("Region = '@This.Region'")]
        public dCity City
        {
            get { return i_City; }
            set { SetPropertyValue("City", ref i_City, value); }
        }
        private dStreet i_Street;
        [DisplayName("Улица")]
        [DataSourceCriteria("City = '@This.City'")]
        public dStreet Street
        {
            get { return i_Street; }
            set { SetPropertyValue("Street", ref i_Street, value); }
        }
        private string i_House;
        [Size(255), DisplayName("Дом")]
        public string House
        {
            get { return i_House; }
            set { SetPropertyValue("House", ref i_House, value); }
        }
        private string i_Office;
        [Size(255), DisplayName("Помещение")]
        public string Office
        {
            get { return i_Office; }
            set { SetPropertyValue("Street", ref i_Office, value); }
        }
    }
}
