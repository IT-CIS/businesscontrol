﻿using BusinessControl.BaseClesses;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Classifiers
{
    [Custom("Caption", "Базовый класс справочников")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dClassifierBase : BaseObjectXAF
    {
        public dClassifierBase(Session session) : base(session) { }

        private string i_Code;
        [Size(32), DisplayName("Код")]
        public string Code
        {
            get { return i_Code; }
            set { SetPropertyValue("Code", ref i_Code, value); }
        }

        private string i_Name;
        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
    }


}
