﻿using BusinessControl.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Classifiers
{
    #region Справочники адреса
    [ModelDefault("Caption", "Регион"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dRegion : TreeNodeAbstract
    {
        public dRegion(Session session) : base(session) { }

        [Association, DisplayName("Муниципальные образования")]
        public XPCollection<dCity> Citys
        {
            get { return GetCollection<dCity>("Citys"); }
        }

        protected override ITreeNode Parent
        {
            get
            {
                return null;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return Citys;
            }
        }

    }

    [ModelDefault("Caption", "Город"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dCity : TreeNodeAbstract
    {
        public dCity(Session session) : base(session) { }

        private dRegion i_Region;
        [Association, DisplayName("Регион")]
        public dRegion Region
        {
            get { return i_Region; }
            set { SetPropertyValue("Region", ref i_Region, value); }
        }

        [Association, DisplayName("Улицы")]
        public XPCollection<dStreet> Streets
        {
            get { return GetCollection<dStreet>("Streets"); }
        }

        protected override ITreeNode Parent
        {
            get
            {
                return Region;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return Streets;
            }
        }
    }

    [ModelDefault("Caption", "Улица"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dStreet : TreeNodeAbstract
    {
        public dStreet(Session session) : base(session) { }

        private dCity i_City;
        [Association, DisplayName("Город")]
        public dCity City
        {
            get { return i_City; }
            set { SetPropertyValue("City", ref i_City, value); }
        }


        protected override ITreeNode Parent
        {
            get
            {
                return City;
            }
        }
        protected override System.ComponentModel.IBindingList Children
        {
            get
            {
                return new System.ComponentModel.BindingList<object>();
            }
        }
    }

    


    #endregion
}
