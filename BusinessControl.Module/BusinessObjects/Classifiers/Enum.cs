﻿using DevExpress.ExpressApp.DC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Enums
{
    /// <summary>
    /// Пол
    /// </summary>
    public enum eSex
    {
        [XafDisplayName("Мужской")]
        male = 0,
        [XafDisplayName("Женский")]
        female = 1
    }

    /// <summary>
    /// Статус клиента
    /// </summary>
    public enum eClientStatus
    {
        [XafDisplayName("Не известно")]
        НеИзвестно = 0,
        [XafDisplayName("Потенциальный заказчик")]
        ПотенциальныйЗаказчик = 1,
        [XafDisplayName("Заказчик")]
        Заказчик = 2,
        [XafDisplayName("Отправлена рассылка")]
        Рассылка = 3
    }

    /// <summary>
    /// Статус задачи
    /// </summary>
    public enum eTaskStatus
    {
        [XafDisplayName("Не началось")]
        НеНачалось = 0,
        [XafDisplayName("Завершено")]
        Завершено = 1,
        [XafDisplayName("Отложено")]
        Отложено = 2,
        [XafDisplayName("Отменено")]
        Отменено = 3,
        [XafDisplayName("Перенесено")]
        Перенесено = 4
    }

    /// <summary>
    /// Вид документа
    /// </summary>
    public enum eDocKind
    {
        [XafDisplayName("Не известно")]
        НеИзвестно = 0,
        [XafDisplayName("Договор")]
        Договор = 1,
        [XafDisplayName("Муниципальный контракт")]
        МуниципальныйКонтракт = 2,
        [XafDisplayName("Коммерческое предложение")]
        КоммерческоеПредложение = 3,
        [XafDisplayName("Акт")]
        Акт = 4,
        [XafDisplayName("Счет")]
        Счет = 5
    }
    /// <summary>
    /// Статус документа
    /// </summary>
    public enum eDocStatus
    {
        [XafDisplayName("Не известно")]
        НеИзвестно = 0,
        [XafDisplayName("Внутреннее согласование")]
        ВнутреннееСогласование = 1,
        [XafDisplayName("Согласован внутри ООО")]
        Согласован = 2,
        [XafDisplayName("Подписан ООО")]
        ПодписанООО = 3,
        [XafDisplayName("Согласование с заказчиком")]
        СогласованиеСЗаказчиком = 4,
        [XafDisplayName("Согласован с заказчиком")]
        СогласованСЗаказчиком = 5,
        [XafDisplayName("Подписан обеими сторонами")]
        Подписан = 6,
        [XafDisplayName("Оплачен")]
        Оплачен = 7,
    }

    /// <summary>
    /// Тип адреса
    /// </summary>
    public enum eAddressType
    {
        [XafDisplayName("Юридический")]
        Юридический = 0,
        [XafDisplayName("Почтовый")]
        Почтовый = 1,
        [XafDisplayName("Местонахождения")]
        Местонахождения = 2,
        [XafDisplayName("Иное")]
        Иное = 3,
    }

}
