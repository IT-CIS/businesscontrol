﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Classifiers
{
    #region Справочники оргструктуры
    [ModelDefault("Caption", "Должность"), NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dPosition : dClassifierBase
    {
        public dPosition(Session session) : base(session) { }

        private string i_NameRod;
        [DisplayName("Должность в род.падеже"), VisibleInListView(false)]
        public string NameRod
        {
            get { return i_NameRod; }
            set { SetPropertyValue("NameRod", ref i_NameRod, value); }
        }

    }

    [ModelDefault("Caption", "Подразделение(отдел)"), NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dDepartment : dClassifierBase
    {
        public dDepartment(Session session) : base(session) { }

    }
    #endregion
}
