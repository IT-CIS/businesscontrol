﻿using BusinessControl.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Classifiers
{
    #region Общие справочники
    [ModelDefault("Caption", "Тип задачи"), NavigationItem("Справочники")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class dTaskType : dClassifierBase
    {
        public dTaskType(Session session) : base(session) { }

    }

    #endregion
}
