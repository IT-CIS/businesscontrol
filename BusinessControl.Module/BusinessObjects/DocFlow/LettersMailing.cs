﻿using BusinessControl.BaseClesses;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.DocFlow
{
    [ModelDefault("Caption", "Email письмо"), NavigationItem("Контрагенты")]
    [System.ComponentModel.DefaultProperty("Subject")]
    public class LettersMailing : AttachBase
    {
        public LettersMailing(Session session) : base(session) {}

        private string i_Subject;
        [Size(255), DisplayName("Тема")]
        public string Subject
        {
            get { return i_Subject; }
            set { SetPropertyValue("Subject", ref i_Subject, value); }
        }

        private string i_MailText;
        [Size(SizeAttribute.Unlimited), DisplayName("Текст письма")]
        public string MailText
        {
            get { return i_MailText; }
            set { SetPropertyValue("MailText", ref i_MailText, value); }
        }

        private DateTime i_LetterDate;
        [DisplayName("Дата")]
        public DateTime LetterDate
        {
            get { return i_LetterDate; }
            set { SetPropertyValue("LetterDate", ref i_LetterDate, value); }
        }

    }
}
