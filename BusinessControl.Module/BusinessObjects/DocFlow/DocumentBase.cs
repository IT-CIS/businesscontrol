﻿using BusinessControl.BaseClesses;
using BusinessControl.Clients;
using BusinessControl.Enums;
using BusinessControl.OrgStructure;
using BusinessControl.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.DocFlow
{
    [NavigationItem("Документооборот"), ModelDefault("Caption", "Документ")]
    public class DocumentBase : AttachBase
    {
        public DocumentBase(Session session) : base(session) { }

        private Employee i_Employee;
        
        private eDocKind i_EDocKind;
        [DisplayName("Вид документа")]
        public eDocKind EDocKind
        {
            get { return i_EDocKind; }
            set { SetPropertyValue("EDocKind", ref i_EDocKind, value); }
        }

        private eDocStatus i_EDocStatus;
        [DisplayName("Статус документа")]
        public eDocStatus EDocStatus
        {
            get { return i_EDocStatus; }
            set { SetPropertyValue("EDocStatus", ref i_EDocStatus, value); }
        }
        private string i_DocNo;
        [Size(64), DisplayName("Номер документа")]
        public string DocNo
        {
            get { return i_DocNo; }
            set { SetPropertyValue("DocNo", ref i_DocNo, value); }
        }
        private DateTime i_StartDocDate;
        [DisplayName("Дата подписания")]
        public DateTime StartDocDate
        {
            get { return i_StartDocDate; }
            set { SetPropertyValue("StartDocDate", ref i_StartDocDate, value); }
        }
        private DateTime i_FinishDocDate;
        [DisplayName("Срок действия")]
        public DateTime FinishDocDate
        {
            get { return i_FinishDocDate; }
            set { SetPropertyValue("FinishDocDate", ref i_FinishDocDate, value); }
        }
        private DateTime i_CreateDocDate;
        [DisplayName("Дата создания")]
        public DateTime CreateDocDate
        {
            get { return i_CreateDocDate; }
            set { SetPropertyValue("CreateDocDate", ref i_CreateDocDate, value); }
        }

        private string i_content;
        [Size(1000), DisplayName("Описание")]
        public string Content
        {
            get { return i_content; }
            set { SetPropertyValue("Content", ref i_content, value); }
        }
        [DisplayName("Инициатор документа")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        private string i_Version;
        [DisplayName("Текущая версия"), VisibleInListView(false)]
        public string Version
        {
            get { return i_Version; }
            set { SetPropertyValue("Version", ref i_Version, value); }
        }

        private Client i_Client;
        [Association, DisplayName("Контрагент")]
        public Client Client
        {
            get { return i_Client; }
            set { SetPropertyValue("Client", ref i_Client, value); }
        }
        private OrgStructure.Organization i_Organization;
        [Association, DisplayName("Организация")]
        public OrgStructure.Organization Organization
        {
            get { return i_Organization; }
            set { SetPropertyValue("Organization", ref i_Organization, value); }
        }

        private string i_RtfText;
        [Size(SizeAttribute.Unlimited), DisplayName("Текст")]
        [EditorAlias("RTF")]
        public string RtfText
        {
            get { return i_RtfText; }
            set { SetPropertyValue("RtfText", ref i_RtfText, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            Connect connect = Connect.FromSession(Session);
            if (i_Employee == null)
                if (SecuritySystem.CurrentUser != null)
                    {
                        Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                        if (currentEmpl != null)
                            i_Employee = currentEmpl;
                    }
            if (CreateDocDate == DateTime.MinValue)
                i_CreateDocDate = DateTime.Now.Date;
        }
    }
}

