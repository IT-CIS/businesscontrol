﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.BaseClesses
{
    public class AttachBase : BaseObjectXAF
    {
        public AttachBase() : base() { }
        public AttachBase(Session session) : base(session) { }

        [Aggregated, Association("AttachBase-AttachmentFiles"), DisplayName("Электронные файлы")]
        //[FileTypeFilter("Файлы", 1, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        //[FileTypeFilter("Изображения", 2, "*.bmp", "*.png", "*.gif", "*.jpg")]
        public XPCollection<AttachmentFiles> AttachmentFiles
        {
            get { return GetCollection<AttachmentFiles>("AttachmentFiles"); }
        }

    }
}
