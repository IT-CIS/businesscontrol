﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.BaseClesses
{
    public class BaseObjectXAF : BaseObject
    {
        public BaseObjectXAF(Session session) : base(session) { }
        public BaseObjectXAF() : base() { }
    }
}
