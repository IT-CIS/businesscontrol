﻿using BusinessControl.BaseClesses;
using BusinessControl.Classifiers;
using BusinessControl.DocFlow;
using BusinessControl.Enums;
using BusinessControl.Interfaces;
using BusinessControl.OrgStructure;
using BusinessControl.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
//using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Clients
{
    /// <summary>
    /// Событие
    /// </summary>
    [ModelDefault("Caption", "Событие")]
    [NavigationItem("Контрагенты")]
    public class BCClientTask : BaseObjectXAF, IBCTask
    {
        public BCClientTask(Session session) : base(session) { }

        private Client i_Client;
        [Association, DisplayName("Контрагент")]
        public Client Client
        {
            get { return i_Client; }
            set { SetPropertyValue("Client", ref i_Client, value); }
        }

        private string i_Subject;
        [Size(255), DisplayName("Тема")]
        public string Subject
        {
            get { return i_Subject; }
            set { SetPropertyValue("Subject", ref i_Subject, value); }
        }

        private string i_Description;
        [Size(SizeAttribute.Unlimited), DisplayName("Описание")]
        public string Description
        {
            get { return i_Description; }
            set { SetPropertyValue("Description", ref i_Description, value); }
        }
        private DateTime i_StartOn;
        [DisplayName("Начало")]
        public DateTime StartOn
        {
            get { return i_StartOn; }
            set { SetPropertyValue("StartOn", ref i_StartOn, value); }
        }
        private DateTime i_DueDate;
        [DisplayName("Срок по задаче")]
        public DateTime DueDate
        {
            get { return i_DueDate; }
            set { SetPropertyValue("DueDate", ref i_DueDate, value); }
        }
        private DateTime i_EndOn;
        [DisplayName("Окончание")]
        public DateTime EndOn
        {
            get { return i_EndOn; }
            set { SetPropertyValue("EndOn", ref i_EndOn, value); }
        }

        private eTaskStatus i_TaskStatus;
        [DisplayName("Статус события")]
        public eTaskStatus TaskStatus
        {
            get { return i_TaskStatus; }
            set { SetPropertyValue("TaskStatus", ref i_TaskStatus, value); }
        }

        private dTaskType i_TaskType;
        [DisplayName("Тип")]
        public dTaskType TaskType
        {
            get { return i_TaskType; }
            set { SetPropertyValue("TaskType", ref i_TaskType, value); }
        }

        private Employee i_Creator;
        [DisplayName("Создатель задачи")]
        public Employee Creator
        {
            get { return i_Creator; }
            set { SetPropertyValue("Creator", ref i_Creator, value); }
        }
        private DateTime i_CreationDate;
        [DisplayName("Дата создания задачи")]
        public DateTime CreationDate
        {
            get { return i_CreationDate; }
            set { SetPropertyValue("CreationDate", ref i_CreationDate, value); }
        }
        private Employee i_Executor;
        [DisplayName("Создатель задачи")]
        public Employee Executor
        {
            get { return i_Executor; }
            set { SetPropertyValue("Executor", ref i_Executor, value); }
        }
        private string i_RegNo;
        [DisplayName("Номер задачи")]
        public string RegNo
        {
            get { return i_RegNo; }
            set { SetPropertyValue("RegNo", ref i_RegNo, value); }
        }

        private LettersMailing i_LetterMail;
        [DisplayName("Ссылка на письмо")]
        public LettersMailing LetterMail
        {
            get { return i_LetterMail; }
            set { SetPropertyValue("LetterMail", ref i_LetterMail, value); }
        }

        public string SetRegNo()
        {
            string res = "";
            if(Client!= null && Client.Name != null && Client.Name != "")
                res = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, Client.Name,string.Empty));
            return res;
        }

        public Employee SetCreator()
        {
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    return currentEmpl;
                else
                    return null;
            }
            else return null;
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (Creator == null)
            {
                try { Creator = SetCreator(); }
                catch { }
            }
        }
    }
}
