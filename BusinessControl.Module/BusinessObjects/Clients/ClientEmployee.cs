﻿using BusinessControl.BaseClesses;
using BusinessControl.Interfaces;
using BusinessControl.OrgStructure;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Clients
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    [ModelDefault("Caption", "Сотрудник заказчика")]
    [NavigationItem("Контрагенты")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    public class ClientEmployee : EmployeeBase
    {
        public ClientEmployee(Session session) : base(session) { }


        private Client i_Client;
        [Association, DisplayName("Контрагент")]
        public Client Client
        {
            get { return i_Client; }
            set { SetPropertyValue("Client", ref i_Client, value); }
        }
    }
}
