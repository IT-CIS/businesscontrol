﻿using BusinessControl.DocFlow;
using BusinessControl.Enums;
using BusinessControl.OrgStructure;
using BusinessControl.SystemDir;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Clients
{
    /// <summary>
    /// Контрагент
    /// </summary>
    [NavigationItem("Контрагенты"), ModelDefault("Caption", "Контрагент")]
    public class Client : OrganizationBase
    {
        public Client(Session session) : base(session) { }

        private eClientStatus i_ClientStatus;
        [DisplayName("Статус")]
        public eClientStatus ClientStatus
        {
            get { return i_ClientStatus; }
            set { SetPropertyValue("ClientStatus", ref i_ClientStatus, value); }
        }

        private bool i_MunicipalFlag;
        [DisplayName("Муниципальный")]
        public bool MunicipalFlag
        {
            get { return i_MunicipalFlag; }
            set { SetPropertyValue("MunicipalFlag", ref i_MunicipalFlag, value); }
        }

        private DateTime i_LastMailingDate;
        [DisplayName("Дата последней рассылки")]
        public DateTime LastMailingDate
        {
            get { return i_LastMailingDate; }
            set { SetPropertyValue("LastMailingDate", ref i_LastMailingDate, value); }
        }

        

        [Association, DisplayName("Сотрудники")]
        public XPCollection<ClientEmployee> ClientEmployees
        {
            get { return GetCollection<ClientEmployee>("ClientEmployees"); }
        }
        [Association, DisplayName("Взаимодействие с клиентом")]
        public XPCollection<BCClientTask> BCClientTasks
        {
            get { return GetCollection<BCClientTask>("BCClientTasks"); }
        }
        [Association, DisplayName("Документы")]
        public XPCollection<DocumentBase> Documents
        {
            get { return GetCollection<DocumentBase>("Documents"); }
        }
    }
}
