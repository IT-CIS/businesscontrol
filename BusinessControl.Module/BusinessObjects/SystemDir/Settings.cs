﻿using DevExpress.Xpo.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace BusinessControl.SystemDir
{
    public static class Settings
    {
        private static bool isLoaded = false;

        public static void Create()
        {
            if (!isLoaded)
            {
                //DbServer = "127.0.0.1";
                //DbLogin = "postgres";
                //DbPassword = "111";
                //DbName = "BusinessControl";
                //Port = 5432;
                //isLoaded = true;
                var doc = XDocument.Load(Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "DBconfig.xml"));

                foreach (var prop in typeof(Settings).GetProperties(BindingFlags.Public | BindingFlags.Static))
                {
                    var node = doc.Root.Descendants().Where(mc => mc.Name.ToString() == prop.Name).SingleOrDefault();
                    if (node != null)
                    {
                        try
                        {
                            var value = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFrom(node.Value);
                            prop.SetValue(null, value);
                        }
                        catch { }
                    }
                }

                isLoaded = true;
            }
        }

        public static void CreateWin()
        {
            if (!isLoaded)
            {
                //DbServer = "127.0.0.1";
                //DbLogin = "postgres";
                //DbPassword = "111";
                //DbName = "BusinessControl";
                //Port = 5432;
                //isLoaded = true;
                try
                {
                    //var request = (HttpWebRequest)WebRequest.Create("http://" + HttpContext.Current.Request.Url.Authority + "/isogd-config.xml");
                    //var response = request.GetResponse();
                    //var doc = XDocument.Load(response.GetResponseStream());
                    //var doc = XDocument.Load(Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "isogd-config.xml"));
                    string assemblyFile =
                    //(new System.Uri(Assembly.GetExecutingAssembly().CodeBase)
                    //).LocalPath;
                    (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)
                        ).LocalPath.Replace("BusinessControl.Module.DLL", "");
                    var doc = XDocument.Load(Path.Combine(assemblyFile, "DBconfig.xml"));
                    foreach (var prop in typeof(Settings).GetProperties(BindingFlags.Public | BindingFlags.Static))
                    {
                        var node = doc.Root.Descendants().Where(mc => mc.Name.ToString() == prop.Name).SingleOrDefault();
                        if (node != null)
                        {
                            try
                            {
                                var value = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFrom(node.Value);
                                prop.SetValue(null, value);
                            }
                            catch { }
                        }
                    }

                    isLoaded = true;
                }
                catch { }
            }
        }

        //для добавления настройки просто добавьте свойство в класс (должно быть открытым статическим)
        //тип данных св-ва любой, приведение автоматическое (если возможно)
        public static string DbServer { get; private set; }
        public static string DbLogin { get; private set; }
        public static string DbPassword { get; private set; }
        public static string DbName { get; private set; }
        public static int Port { get; private set; }

        //public static string ArcGisHostName { get; private set; }
        public static string GetDbConnectionString()
        {
            //return MSSqlConnectionProvider.GetConnectionString(DbServer, DbLogin, DbPassword.ToString(), DbName);
            return PostgreSqlConnectionProvider.GetConnectionString(DbServer, Port, DbLogin, DbPassword.ToString(), DbName);
        }
    }
}
