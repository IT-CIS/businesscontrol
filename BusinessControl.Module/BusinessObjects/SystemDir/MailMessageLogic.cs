﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.SystemDir
{
    public class MailMessageLogic
    {
        public static string host = "smtp.yandex.ru";
        public static int port = 587;//465;//587;
        public static string login = "dmitry@it-cis.ru";
        public static string pass = "cfv217ceyu";

        public static void SendMailMessage(string title, string bodyMessage, string fileAttachmentNames, string too, string from, bool isBodyHTML)
        {

            SmtpClient client = new SmtpClient();
            client.Port = port;
            client.Host = host;
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(login, pass);


            MailMessage mm = new MailMessage();
            mm.From = new MailAddress(from);
            foreach (string to in too.Split(','))
            {
                mm.To.Add(to);
            }
            mm.Subject = title;
            mm.Body = bodyMessage;
            if (fileAttachmentNames != "")
            {
                foreach (string fileAttachmentName in fileAttachmentNames.Split(','))
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(fileAttachmentName);
                    mm.Attachments.Add(attachment);
                }
            }
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mm.IsBodyHtml = isBodyHTML;
            
            client.Send(mm);

            //MailMessage mail = new MailMessage();
            //SmtpClient SmtpServer = new SmtpClient("smtp.yandex.ru");
            //mail.From = new MailAddress("dmitry@it-cis.ru");
            //mail.To.Add("info@it-cis.ru");
            //mail.Subject = "Test Mail - 1";
            //mail.Body = "mail with attachment";

            //System.Net.Mail.Attachment attachment;
            //attachment = new System.Net.Mail.Attachment(fileAttachmentName);
            //mail.Attachments.Add(attachment);

            //SmtpServer.Port = 587;
            //SmtpServer.Credentials = new System.Net.NetworkCredential("dmitry@it-cis.ru", "");
            //SmtpServer.EnableSsl = true;
            ////SmtpServer.Timeout = 100000;
            //SmtpServer.Send(mail);
        }
    }
}
