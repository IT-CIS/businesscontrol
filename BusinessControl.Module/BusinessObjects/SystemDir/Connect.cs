﻿using BusinessControl.Module;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.SystemDir
{
    public class Connect
    {
        private Connect(Session session)
        {
            this.session = session;
        }

        public static Connect FromSession(Session session)
        {
            return new Connect(session);
        }

        public static Connect FromUnitOfWork(UnitOfWork unitOfWork)
        {
            //сделано просто для наглядности и для незнающих иерархии наследовния (UnitOfWork : Session)
            return new Connect(unitOfWork);
        }

        public static Connect FromObjectSpace(IObjectSpace iObjectSpace)
        {
            if (iObjectSpace is XPObjectSpace)
                return new Connect((iObjectSpace as XPObjectSpace).Session);
            else
                throw new ArgumentException("iObjectSpace не является объектом класса XPObjectSpace");
        }

        public static Connect FromSettings()
        {
            if (settingsConnect == null)
            {
                XpoDefault.DataLayer = XpoDefault.GetDataLayer(Settings.GetDbConnectionString(),
                        DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema);
                settingsConnect = new Connect(new Session());
            }
            return settingsConnect;
        }

        Session session;
        static Connect settingsConnect = null;
        UnitOfWork unitOfWork;

        public Session GetSession()
        {
            return session;
        }

        //---------------------------------------------------------------------------------

        public void SaveObject(object @object)
        {
            if (@object is XPBaseObject)
                ((XPBaseObject)@object).Save();
            session.Save(@object);
        }

        //---------------------------------------------------------------------------------

        public void DeleteObject(object @object)
        {
            session.Delete(@object);
        }

        public void DeleteObjects(ICollection objects)
        {
            session.Delete(objects);
        }

        //---------------------------------------------------------------------------------

        public XPClassInfo GetClassInfo<T>()
        {
            return session.GetClassInfo<T>();
        }

        public XPClassInfo GetClassInfo(string className)
        {
            return session.GetClassInfo(typeof(BusinessControlModule).Assembly.FullName, className);
        }

        //---------------------------------------------------------------------------------

        public XPQuery<T> Query<T>()
        {
            return session.Query<T>();
        }

        //---------------------------------------------------------------------------------

        public bool IsEmpty<T>()
        {
            return !session.Query<T>().Any();
        }

        //-------------------------------

        public bool IsExist<T>(Expression<Func<T, bool>> expression)
        {
            return session.Query<T>().Any(expression);
        }

        public bool IsExist(XPClassInfo classInfo, string condition)
        {
            if (session.FindObject(classInfo, CriteriaOperator.Parse(condition)) != null)
                return true;
            return false;
        }

        //-------------------------------

        public T GetObjectByKey<T>(object id)
        {
            return session.GetObjectByKey<T>(id);
        }

        public XPBaseObject GetObjectByKey(XPClassInfo classInfo, object id)
        {
            return session.GetObjectByKey(classInfo, id) as XPBaseObject;
        }

        //-------------------------------

        public T FindFirstObject<T>(Expression<Func<T, bool>> expression)
        {
            return session.Query<T>().FirstOrDefault(expression);
        }

        public XPBaseObject FindFirstObject(XPClassInfo classInfo, string condition)
        {
            return session.FindObject(classInfo, CriteriaOperator.Parse(condition)) as XPBaseObject;
        }

        //-------------------------------

        public IQueryable<T> FindObjects<T>(Expression<Func<T, bool>> expression)
        {
            return session.Query<T>().Where(expression);
        }

        public IEnumerable<XPBaseObject> FindObjects(XPClassInfo classInfo, string condition)
        {
            return session.GetObjects(classInfo, CriteriaOperator.Parse(condition), null, 0, false, false).Cast<XPBaseObject>();
        }

        //-------------------------------

        public T CreateObject<T>()
        {
            return (T)GetClassInfo<T>().CreateNewObject(session);
        }

        public XPBaseObject CreateObject(XPClassInfo classInfo)
        {
            return classInfo.CreateNewObject(session) as XPBaseObject;
        }

        public XPBaseObject CreateObject(string className)
        {
            return GetClassInfo(className).CreateNewObject(session) as XPBaseObject;
        }

        public UnitOfWork GetUnitOfWork()
        {
            return unitOfWork = (UnitOfWork)session;
        }

        //-------------------------------
    }
}
