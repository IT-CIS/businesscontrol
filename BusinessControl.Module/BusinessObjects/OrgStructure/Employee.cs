﻿using BusinessControl.BaseClesses;
using BusinessControl.Interfaces;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.OrgStructure
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    [ModelDefault("Caption", "Сотрудник")]
    [NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    public class Employee : EmployeeBase
    {
        public Employee(Session session) : base(session) { }

        private PermissionPolicyUser i_user;
        [DisplayName("Пользователь системы")]
        public PermissionPolicyUser SysUser
        {
            get { return i_user; }
            set { SetPropertyValue("SysUser", ref i_user, value); }
        }

        [Association, DisplayName("Организации")]
        public XPCollection<Organization> Organizations
        {
            get { return GetCollection<Organization>("Organizations"); }
        }
    }
}
