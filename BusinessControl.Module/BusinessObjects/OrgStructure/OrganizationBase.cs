﻿using BusinessControl.Address;
using BusinessControl.BaseClesses;
using BusinessControl.Classifiers;
using BusinessControl.Interfaces;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.OrgStructure
{
    /// <summary>
    /// Юридическое лицо
    /// </summary>
    [ModelDefault("Caption", "Организация")]
    //[NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    public class OrganizationBase : BaseObjectXAF, IOrganization
    {
        public OrganizationBase(Session session) : base(session) { }

        [DisplayName("Лого")]
        [Size(SizeAttribute.Unlimited), VisibleInListView(false)]
        [ImageEditor(DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
            DetailViewImageEditorFixedHeight = 300, DetailViewImageEditorFixedWidth = 300)]
        public byte[] Logo
        {
            get { return GetPropertyValue<byte[]>("Logo"); }
            set { SetPropertyValue<byte[]>("Logo", value); }
        }

        private string i_Name;
        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }

        
        /// <summary>
        /// Контактные данные
        /// </summary>
        private string i_Phones;
        [Size(255), DisplayName("Телефоны")]
        public string Phones
        {
            get { return i_Phones; }
            set { SetPropertyValue("Phones", ref i_Phones, value); }
        }

        private string i_Email;
        [Size(255), DisplayName("Email")]
        public string Email
        {
            get { return i_Email; }
            set { SetPropertyValue("Email", ref i_Email, value); }
        }

        private string i_Address;
        [Size(255), DisplayName("Адрес")]
        public string Address
        {
            get { return i_Address; }
            set { SetPropertyValue("Address", ref i_Address, value); }
        }

        

        /// <summary>
        /// Реквизиты
        /// </summary>
        private string i_INN;
        [Size(255), DisplayName("ИНН")]
        public string INN
        {
            get { return i_INN; }
            set { SetPropertyValue("INN", ref i_INN, value); }
        }
        private string i_KPP;
        [Size(255), DisplayName("КПП")]
        public string KPP
        {
            get { return i_KPP; }
            set { SetPropertyValue("KPP", ref i_KPP, value); }
        }

        private string i_Notes;
        [Size(SizeAttribute.Unlimited), DisplayName("Доп. данные")]
        public string Notes
        {
            get { return i_Notes; }
            set { SetPropertyValue("Notes", ref i_Notes, value); }
        }

        [Association, DisplayName("Адреса")]
        public XPCollection<AddressBase> AddressBases
        {
            get { return GetCollection<AddressBase>("AddressBases"); }
        }

    }
}
