﻿using BusinessControl.BaseClesses;
using BusinessControl.Classifiers;
using BusinessControl.Interfaces;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.OrgStructure
{
    /// <summary>
    /// Базовый класс сотрудника
    /// </summary>
    [ModelDefault("Caption", "Базовый класс сотрудника")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    public class EmployeeBase : BaseObjectXAF, IEmployee
    {
        public EmployeeBase(Session session) : base(session) { }

        private string i_LastName;
        [Size(255), DisplayName("Фамилия")]
        //[ImmediatePostData]
        public string LastName
        {
            get { return i_LastName; }
            set { SetPropertyValue("LastName", ref i_LastName, value); }
        }
        private string i_FirstName;
        [Size(255), DisplayName("Имя")]
        //[ImmediatePostData]
        public string FirstName
        {
            get { return i_FirstName; }
            set { SetPropertyValue("FirstName", ref i_FirstName, value); }
        }
        private string i_MiddleName;
        [Size(255), DisplayName("Отчество")]
        //[ImmediatePostData]
        public string MiddleName
        {
            get { return i_MiddleName; }
            set { SetPropertyValue("MiddleName", ref i_MiddleName, value); }
        }
        private string i_FullName;
        [Size(255), DisplayName("ФИО")]
        public string FullName
        {
            get
            {
                //i_FullName = GetFullName();
                return i_FullName;
            }
            set
            {
                SetPropertyValue("FullName", ref i_FullName, value);
            }
        }
        public string GetFullName()
        {
            string res = "";
            try
            {
                res = String.Format("{0}", LastName);
            }
            catch { }
            try
            {
                res += String.Format(" {0}", FirstName);
            }
            catch { }
            try
            {
                res += String.Format(" {0}", MiddleName);
            }
            catch { }
            res = res.Trim();

            return res;
        }

        public string GetBriefName()
        {
            string res = "";
            try
            {
                res = String.Format("{0}", LastName);
            }
            catch { }
            try
            {
                res += String.Format(" {0}.", FirstName.Substring(0,1));
            }
            catch { }
            try
            {
                res += String.Format(" {0}.", MiddleName.Substring(0, 1));
            }
            catch { }
            res = res.Trim();

            return res;
        }

        private string i_BriefName;
        [Size(255), DisplayName("Фамилия, инициалы")]
        public string BriefName
        {
            get {
                //i_BriefName = GetBriefName();
                return i_BriefName; }
            set { SetPropertyValue("BriefName", ref i_BriefName, value); }
        }

        private string i_NameRod;
        [Size(255), DisplayName("ФИО в род.падеже"), VisibleInListView(false)]
        public string NameRod
        {
            get {
                //i_NameRod = GetFullName();
                return i_NameRod; }
            set { SetPropertyValue("NameRod", ref i_NameRod, value); }
        }

        private string i_Phones;
        [Size(255), DisplayName("Телефоны")]
        public string Phones
        {
            get { return i_Phones; }
            set { SetPropertyValue("Phones", ref i_Phones, value); }
        }

        private string i_Email;
        [Size(255), DisplayName("Email")]
        public string Email
        {
            get { return i_Email; }
            set { SetPropertyValue("Email", ref i_Email, value); }
        }

        private dPosition i_Position;
        [DisplayName("Должность")]
        public dPosition Position
        {
            get { return i_Position; }
            set { SetPropertyValue("Position", ref i_Position, value); }
        }

        private bool i_isSignPerson;
        [DisplayName("Право подписи")]
        [ImmediatePostData]
        public bool isSignPerson
        {
            get { return i_isSignPerson; }
            set { SetPropertyValue("isSignPerson", ref i_isSignPerson, value);
                GetActionBased();
            }
        }

        private string i_ActingBased;
        [DisplayName("Действующего на основании")]
        public string ActingBased
        {
            get { return i_ActingBased; }
            set { SetPropertyValue("ActingBased", ref i_ActingBased, value); }
        }

        private void GetActionBased()
        {
            string res = "";
            if (isSignPerson && String.IsNullOrEmpty(ActingBased))
                i_ActingBased = "Устава";
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            //i_FullName = GetFullName();
            //if (String.IsNullOrEmpty(i_NameRod))
            //    i_NameRod = i_FullName;
        }
    }
}
