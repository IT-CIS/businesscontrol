﻿using BusinessControl.DocFlow;
using BusinessControl.Enums;
using BusinessControl.OrgStructure;
using BusinessControl.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.OrgStructure
{
    /// <summary>
    /// Организация
    /// </summary>
    [ModelDefault("Caption", "Организация")]
    [NavigationItem("Структура организации")]
    public class Organization : OrganizationBase
    {
        public Organization(Session session) : base(session) { }

        [DisplayName("Печать")]
        [Size(SizeAttribute.Unlimited), VisibleInListView(false)]
        [ImageEditor(DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
            DetailViewImageEditorFixedHeight = 190, DetailViewImageEditorFixedWidth = 190)]
        public byte[] Stamp
        {
            get { return GetPropertyValue<byte[]>("Stamp"); }
            set { SetPropertyValue<byte[]>("Stamp", value); }
        }

        [DisplayName("Подпись")]
        [Size(SizeAttribute.Unlimited), VisibleInListView(false)]
        [ImageEditor(DetailViewImageEditorMode = ImageEditorMode.PictureEdit,
            DetailViewImageEditorFixedHeight = 57, DetailViewImageEditorFixedWidth = 100)]
        public byte[] Signature
        {
            get { return GetPropertyValue<byte[]>("Signature"); }
            set { SetPropertyValue<byte[]>("Signature", value); }
        }

        private Employee i_Employee;
        [DisplayName("Подписывающее лицо")]
        [DataSourceProperty("AvailableEmployees")]
        public Employee Employee
        {
            get { return i_Employee; }
            set { SetPropertyValue("Employee", ref i_Employee, value); }
        }

        // Получаем возможные для выбора территориальные зоны
        private XPCollection<Employee> availableEmployees;
        [System.ComponentModel.Browsable(false)] //
        public XPCollection<Employee> AvailableEmployees
        {
            get
            {
                if (availableEmployees == null)
                {
                    // Retrieve all Terminals objects 
                    availableEmployees = new XPCollection<Employee>(Session);
                    // Filter the retrieved collection according to current conditions 
                    RefreshAvailableEmployees();
                }
                return availableEmployees;
            }
        }
        private void RefreshAvailableEmployees()
        {
            if (availableEmployees == null)
                return;
            if (Employees.Count >= 1)
            {
                availableEmployees.Criteria = new InOperator("Oid", GetAvailableEmployees());
                try {
                    if(Employee == null)
                        Employee = availableEmployees.FirstOrDefault(); }
                catch { }
            }
            else
            {
                availableEmployees = new XPCollection<Employee>(Session);
            }
        }
        private List<string> GetAvailableEmployees()
        {
            List<string> AvailableEmployeesList = new List<string>();
            foreach (Employee employee in Employees)
            {
                try
                {
                    if (employee.isSignPerson)
                        AvailableEmployeesList.Add(employee.Oid.ToString());
                }
                catch { }
            }
            return AvailableEmployeesList;
        }

        [Association, DisplayName("Сотрудники")]
        public XPCollection<Employee> Employees
        {
            get { return GetCollection<Employee>("Employees"); }
        }
        [Association, DisplayName("Документы")]
        public XPCollection<DocumentBase> Documents
        {
            get { return GetCollection<DocumentBase>("Documents"); }
        }
    }
}
