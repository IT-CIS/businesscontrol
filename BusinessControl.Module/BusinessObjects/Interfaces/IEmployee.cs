﻿using BusinessControl.BaseClesses;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Interfaces
{
    /// <summary>
    /// Юридическое лицо
    /// </summary>
    public interface IEmployee : IPersonalData, IContactData
    {

        //IOrganization Org { get; set; }

    }
}
