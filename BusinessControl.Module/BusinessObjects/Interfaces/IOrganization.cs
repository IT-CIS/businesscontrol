﻿using BusinessControl.BaseClesses;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Interfaces
{
    /// <summary>
    /// Юридическое лицо
    /// </summary>
    public interface IOrganization : IContactData
    {
        string Name { get; set; }

    }
}
