﻿using BusinessControl.BaseClesses;
using BusinessControl.Classifiers;
using BusinessControl.Enums;
using BusinessControl.OrgStructure;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Interfaces
{
    /// <summary>
    /// Интерфейс задачи
    /// </summary>
    public interface IBCTask
    {

        string Subject { get; set; }
        string Description { get; set; }
        DateTime StartOn { get; set; }
        DateTime EndOn { get; set; }
        DateTime DueDate { get; set; }
        eTaskStatus TaskStatus { get; set; }
        dTaskType TaskType { get; set; }
        Employee Creator { get; set; }
        DateTime CreationDate { get; set; }
        string RegNo { get; set; }
        Employee Executor { get; set; }
        string SetRegNo();
        Employee SetCreator();
    }
}
