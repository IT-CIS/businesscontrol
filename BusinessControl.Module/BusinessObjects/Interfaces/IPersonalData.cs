﻿using BusinessControl.BaseClesses;
using BusinessControl.Enums;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Interfaces
{
    /// <summary>
    /// Интерфейс персональных данных сотрудника
    /// </summary>
    public interface IPersonalData
    {

        string FullName { get; set; }
        string LastName { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string BriefName { get; set; }
        //string Phones { get; set; }
        //string Email { get; set; }
        //ESex Sex {get; set;}

    }
}
