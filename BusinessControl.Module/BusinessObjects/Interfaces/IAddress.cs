﻿using BusinessControl.BaseClesses;
using BusinessControl.Classifiers;
using BusinessControl.Enums;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Interfaces
{
    /// <summary>
    /// Интерфейс персональных данных сотрудника
    /// </summary>
    public interface IAddress
    {
        string Index { get; set; }
        dRegion Region { get; set; }
        dCity City { get; set; }
        dStreet Street { get; set; }
        string House { get; set; }
        string Office { get; set; }
        //void GetFullName();

    }
}
