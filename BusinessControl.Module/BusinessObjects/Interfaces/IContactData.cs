﻿using BusinessControl.BaseClesses;
using BusinessControl.Enums;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessControl.Interfaces
{
    /// <summary>
    /// Интерфейс персональных данных сотрудника
    /// </summary>
    public interface IContactData
    {
        string Phones { get; set; }
        string Email { get; set; }
    }
}
