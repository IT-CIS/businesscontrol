﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using BusinessControl.DocFlow;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using BusinessControl.SystemDir;
using BusinessControl.BaseClesses;
using BusinessControl.Clients;
using System.IO;
using BusinessControl.Classifiers;

namespace BusinessControl.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SendMailToClientsController : ViewController
    {
        public SendMailToClientsController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        string emails = "";
        LettersMailing lettersMailing;
        Connect connect; 
        /// <summary>
        /// Отправить письмо выбранным клиентам
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendMailToClientsAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();
            IObjectSpace os = Application.CreateObjectSpace();

            
            var x_id = ((BaseObject)e.CurrentObject).Oid;

            lettersMailing = os.FindObject<LettersMailing>(new BinaryOperator("Oid", x_id));
            UnitOfWork unitOfWork = (UnitOfWork)lettersMailing.Session;
            connect = Connect.FromUnitOfWork(unitOfWork);


            CollectionSourceBase source = Application.CreateCollectionSource(Application.CreateObjectSpace(), typeof(Client), "Client_ListView");
            string ID = Application.FindListViewId(typeof(Client));
            e.ShowViewParameters.CreatedView = Application.CreateListView(ID, source, true);
            e.ShowViewParameters.CreateAllControllers = true;
            
            //e.ShowViewParameters.CreatedView.SelectionType(SelectionType.MultipleSelection);
            //e.ShowViewParameters.CreatedView.Caption = constr.name;
            DialogController contr = new DialogController();
            contr.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(contr_Accepting);
            e.ShowViewParameters.Controllers.Add(contr);
            //throw new Exception("2");
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            os.CommitChanges();

            
        }

        // при выборе клиентов и нажатии на кнопку Ок
        void contr_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            if (e.AcceptActionArgs.SelectedObjects.Count == 0)
            {
                throw new Exception("Не выбран ни один клиент.");
            }
            string paths = "";
            foreach (AttachmentFiles attach in lettersMailing.AttachmentFiles)
            {
                string path = Path.GetTempPath() + @"\" + attach.File.FileName;
                using (FileStream fs = File.Create(path))
                {
                    MemoryStream ms = new MemoryStream();
                    attach.File.SaveToStream(ms);
                    ms.Seek(0, System.IO.SeekOrigin.Begin);
                    ms.CopyTo(fs);
                    ms.Dispose();
                    fs.Dispose();
                    fs.Close();
                }
                paths += path + ",";
            }
            paths = paths.TrimEnd(',');

            for (int i = 0; i < e.AcceptActionArgs.SelectedObjects.Count; i++)
            {
                Client i_Client = (Client)e.AcceptActionArgs.SelectedObjects[i];
                Client client = connect.FindFirstObject<Client>(x=> x.Oid == i_Client.Oid);


                if(client.Email!= null && client.Email != "")
                {
                    string titleClient = lettersMailing.Subject;
                    string messbody = lettersMailing.MailText;
                    
                    string email = client.Email;
                    try
                    {
                        MailMessageLogic.SendMailMessage(titleClient, messbody, paths, email, "dmitry@it-cis.ru", true);
                        // создаем задачу у клиента
                        //try {
                        CreateClientTask(connect, client, lettersMailing);
                    }
                    catch { }
                    //}
                    //catch { }
                }
                    //emails += client.Email + "," ;
            }
            //emails = emails.TrimEnd(',');
            //string titleClient = lettersMailing.Subject;
            //string messbody = lettersMailing.MailText;
            //string paths = "";
            //foreach (AttachmentFiles attach in lettersMailing.AttachmentFiles)
            //{
            //    string path = Path.GetTempPath() + @"\" + attach.File.FileName;
            //    using (FileStream fs = File.Create(path))
            //    {
            //        MemoryStream ms = new MemoryStream();
            //        attach.File.SaveToStream(ms);
            //        ms.Seek(0, System.IO.SeekOrigin.Begin);
            //        ms.CopyTo(fs);
            //        ms.Dispose();
            //        fs.Dispose();
            //        fs.Close();
            //    }
            //    paths += path + ",";
            //}
            //paths = paths.TrimEnd(',');

            //foreach (string email in emails.Split(','))
            //{
            //    //try
            //    //{
            //    MailMessageLogic.SendMailMessage(titleClient, messbody, paths, email, "donotreply @it-cis.ru", true);
            //    //}
            //    //catch { }
            //}

            //foreach(string p in paths.Split(','))
            //{
            //    File.Delete(p);
            //}
        }
        void CreateClientTask(Connect connect, Client client, LettersMailing letter)
        {
            BCClientTask clientTask = connect.CreateObject<BCClientTask>();
            clientTask.Client = client;
            clientTask.CreationDate = DateTime.Now.Date;
            try { clientTask.Creator = clientTask.SetCreator(); }
            catch { }
            clientTask.Description = "Рассылка писем с коммерческим предожением клиентам";
            clientTask.Subject = "Рассылка писем с коммерческим предожением клиентам";
            clientTask.EndOn = DateTime.Now.Date;
            try
            {
                clientTask.Executor = clientTask.SetCreator();
            }
            catch { }
            clientTask.LetterMail = letter;
            try
            {
                clientTask.RegNo = clientTask.SetRegNo();
            }
            catch { }
            clientTask.StartOn = DateTime.Now.Date;
            clientTask.TaskStatus = Enums.eTaskStatus.Завершено;
            try
            {
                clientTask.TaskType = connect.FindFirstObject<dTaskType>(x => x.Name == "Email-письмо");
            }
            catch { }
            clientTask.Save();
            client.ClientStatus = Enums.eClientStatus.Рассылка;
            client.LastMailingDate = DateTime.Now.Date;
            client.Save();
            connect.GetUnitOfWork().CommitChanges();
        }
    }
}
