﻿namespace BusinessControl.Module.Controllers
{
    partial class SetEmployeeFullNameController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SetEmployeeFullNameAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SetEmployeeFullNameAction
            // 
            this.SetEmployeeFullNameAction.Caption = "Заполнить ФИО";
            this.SetEmployeeFullNameAction.Category = "SetEmployeeFullNameCategory";
            this.SetEmployeeFullNameAction.ConfirmationMessage = null;
            this.SetEmployeeFullNameAction.Id = "SetEmployeeFullNameAction";
            this.SetEmployeeFullNameAction.ToolTip = "Заполнение полей ФИО по введенным данным";
            this.SetEmployeeFullNameAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SetEmployeeFullNameAction_Execute);
            // 
            // SetEmployeeFullNameController
            // 
            this.Actions.Add(this.SetEmployeeFullNameAction);
            this.TargetObjectType = typeof(BusinessControl.OrgStructure.EmployeeBase);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SetEmployeeFullNameAction;
    }
}
