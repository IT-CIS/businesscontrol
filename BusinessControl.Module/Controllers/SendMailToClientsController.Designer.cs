﻿namespace BusinessControl.Module.Controllers
{
    partial class SendMailToClientsController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SendMailToClientsAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SendMailToClientsAction
            // 
            this.SendMailToClientsAction.Caption = "Отправить письмо";
            this.SendMailToClientsAction.ConfirmationMessage = null;
            this.SendMailToClientsAction.Id = "SendMailToClientsAction";
            this.SendMailToClientsAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.SendMailToClientsAction.TargetObjectType = typeof(BusinessControl.DocFlow.LettersMailing);
            this.SendMailToClientsAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.SendMailToClientsAction.ToolTip = null;
            this.SendMailToClientsAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.SendMailToClientsAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SendMailToClientsAction_Execute);
            // 
            // SendMailToClientsController
            // 
            this.Actions.Add(this.SendMailToClientsAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SendMailToClientsAction;
    }
}
