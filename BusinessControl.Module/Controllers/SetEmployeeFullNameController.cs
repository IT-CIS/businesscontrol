﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessControl.OrgStructure;
using BusinessControl.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace BusinessControl.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SetEmployeeFullNameController : ViewController
    {
        public SetEmployeeFullNameController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SetEmployeeFullNameAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            View.ObjectSpace.CommitChanges();

            IObjectSpace os = Application.CreateObjectSpace();

            string empId = ((BaseObject)View.CurrentObject).Oid.ToString();

            Connect connect = Connect.FromObjectSpace(os);

            EmployeeBase employee = connect.FindFirstObject<EmployeeBase>(x => x.Oid.ToString() == empId);

            employee.FullName = employee.GetFullName();
            employee.BriefName = employee.GetBriefName();
            employee.NameRod = employee.GetFullName();

            employee.Save();
            os.CommitChanges();
            RefreshFrame();
        }

        protected void RefreshFrame()
        {
            RefreshController controller = Frame.GetController<RefreshController>();
            if (controller != null)
                controller.RefreshAction.DoExecute();
        }
    }
}
