﻿namespace BusinessControl.Module.Controllers
{
    partial class EditAttachFileAsRTFViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.EditAttachFileAsRTFAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // EditAttachFileAsRTFAction
            // 
            this.EditAttachFileAsRTFAction.Caption = "Открыть";
            this.EditAttachFileAsRTFAction.Category = "ListView";
            this.EditAttachFileAsRTFAction.ConfirmationMessage = null;
            this.EditAttachFileAsRTFAction.Id = "EditAttachFileAsRTFAction";
            this.EditAttachFileAsRTFAction.ImageName = "BO_Document";
            this.EditAttachFileAsRTFAction.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.EditAttachFileAsRTFAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.EditAttachFileAsRTFAction.ToolTip = "Открыть документ на радактирование";
            this.EditAttachFileAsRTFAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.EditAttachFileAsRTFAction_Execute);
            // 
            // EditAttachFileAsRTFViewController
            // 
            this.Actions.Add(this.EditAttachFileAsRTFAction);
            this.TargetObjectType = typeof(BusinessControl.BaseClesses.AttachmentFiles);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction EditAttachFileAsRTFAction;
    }
}
