﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using BusinessControl.BaseClesses;
using BusinessControl.DocFlow;
using BusinessControl.SystemDir;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.XtraRichEdit;

namespace BusinessControl.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class EditAttachFileAsRTFViewController : ViewController
    {
        ActionUrl actionUrl;
        AttachmentFiles attach;

        public EditAttachFileAsRTFViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            actionUrl = new ActionUrl(this, "OpenDocument1", "ListView");
        }

        private void EditAttachFileAsRTFAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            attach = (AttachmentFiles)View.CurrentObject;
            //BusinessControl.Module.Web.Controllers.OpentDocumentFileController
            //string relativeUrl = string.Format(CultureInfo.InvariantCulture, "~/OpenDocument.aspx?oid={0}", attach.Oid);
            //actionUrl.UrlFormatString = WebWindow.CurrentRequestPage.ResolveUrl(relativeUrl);
            //View.Refresh(true);
            //WebApplication.Instance.ObjectSpaceCreated += Instance_ObjectSpaceCreated;

            //OpentDocumentFileController
            actionUrl.UrlFieldName = "Oid";
            actionUrl.UrlFormatString = "http://localhost:62345/OpenDocument.aspx?oid={0}";
            actionUrl.SelectionDependencyType = SelectionDependencyType.RequireSingleObject;

        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

        }
        private void Instance_ObjectSpaceCreated(object sender, ObjectSpaceCreatedEventArgs e)
        {
            e.ObjectSpace.ObjectChanged += ObjectSpace_ObjectChanged;
        }

        private void ObjectSpace_ObjectChanged(object sender, ObjectChangedEventArgs e)
        {
            if (e.Object is FileData)
            {
                var fileData = (FileData)e.Object;
                if (fileData.Oid == attach.File.Oid)
                {
                    View.Refresh(true);
                }
            }
        }

        protected override void OnDeactivated()
        {
            WebApplication.Instance.ObjectSpaceCreated -= Instance_ObjectSpaceCreated;
            base.OnDeactivated();
        }

        //private void EditAttachFileAsRTFAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        //{
        //    IObjectSpace os = Application.CreateObjectSpace();


        //    var x_id = ((BaseObject)e.CurrentObject).Oid;

        //    AttachmentFiles attach = os.FindObject<AttachmentFiles>(new BinaryOperator("Oid", x_id));
        //    Connect connect = Connect.FromObjectSpace(os);

        //    if(attach.AttachBase!= null && attach.AttachBase.GetType()== typeof(DocumentBase))
        //    {
        //        DocumentBase document = (DocumentBase)attach.AttachBase;

        //        if(attach.File != null)
        //        {
        //            document.RtfText = "";

        //            string path = @"C:\Temp\" + attach.File.FileName;  //Path.GetTempPath() + @"C:\Temp\" + attach.File.FileName;
        //            using (FileStream fs = File.Create(path))
        //            {
        //                MemoryStream ms = new MemoryStream();
        //                attach.File.SaveToStream(ms);
        //                ms.Seek(0, System.IO.SeekOrigin.Begin);
        //                ms.CopyTo(fs);
        //                ms.Dispose();
        //                fs.Dispose();
        //                fs.Close();
        //            }

        //            RichEditDocumentServer server = new RichEditDocumentServer();
        //            server.LoadDocument(path, DocumentFormat.OpenXml);

        //            string path1 = @"C:\Temp\" + attach.File.FileName + ".rtf";  //Path.GetTempPath() + @"\" + attach.File.FileName + ".rtf";

        //            server.SaveDocument(path1, DocumentFormat.Rtf);

        //            StreamReader rtfStreamReader = new StreamReader(path1);
        //            document.RtfText = rtfStreamReader.ReadToEnd();
        //            document.Save();
        //            os.CommitChanges();



        //            //MemoryStream ms = new MemoryStream();
        //            //attach.File.SaveToStream(ms);
        //            //ms.Seek(0, System.IO.SeekOrigin.Begin);
        //            //StreamReader rtfStreamReader = new StreamReader(path);
        //            //document.RtfText = rtfStreamReader.ReadToEnd();
        //            //document.Save();
        //            //os.CommitChanges();

        //            //ms.Close();
        //            //ms.Dispose();
                    
        //            rtfStreamReader.Close();
        //            rtfStreamReader.Dispose();


        //            //string path = Path.GetTempPath() + @"\" + attach.File.FileName;

        //           //StreamReader rtfStreamReader = new StreamReader(ms);

        //           // attach.File.SaveToStream(rtfStreamReader);
        //           // document.RtfText = rtfStreamReader.ReadToEnd();
        //           // document.Save();
        //           // os.CommitChanges();

        //           // using (FileStream fs = File.Create(path))
        //           // {
        //           //     MemoryStream ms = new MemoryStream();
        //           //     attach.File.SaveToStream(ms);
        //           //     ms.Seek(0, System.IO.SeekOrigin.Begin);
        //           //     ms.CopyTo(fs);
        //           //     ms.Dispose();
        //           //     fs.Dispose();
        //           //     fs.Close();
        //           // }
        //        }
        //    }
        //}
    }
}
