﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpenDocument.aspx.cs" Inherits="DxSample.Web.OpenDocument" %>

<%@ Register assembly="DevExpress.Web.ASPxRichEdit.v18.1, Version=18.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRichEdit" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <%--<dx:ASPxRichEdit ID="RichEditor" runat="server" WorkDirectory="~\App_Data\WorkDirectory" Theme="Aqua">
        </dx:ASPxRichEdit>--%>
    <dx:ASPxRichEdit ID="RichEditor" runat="server" Height="800px"  Width="100%" OnSaving="RichEditor_Saving" WorkDirectory="~\App_Data\WorkDirectory">
            <ClientSideEvents Init="function(s, e){ s.toggleFullScreenMode(); }" />
        </dx:ASPxRichEdit>
    </div>
    </form>
</body>
</html>
