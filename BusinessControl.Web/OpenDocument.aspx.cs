﻿using System;
using BusinessControl.BaseClesses;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Web;
using DevExpress.Persistent.BaseImpl;
//using DevExpress.Persistent.BaseImpl.EF;
using DevExpress.XtraRichEdit;

namespace DxSample.Web {
    public partial class OpenDocument : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {

            //int fileDataId = int.Parse(this.Request.QueryString["fileDataId"]);
            //IObjectSpace objectSpace = WebApplication.Instance.CreateObjectSpace(typeof(FileData));
            //FileData fileData = objectSpace.GetObjectByKey<FileData>(fileDataId);
            //this.RichEditor.Open(fileData.FileName, DocumentFormat.Rtf, () => fileData.Content);

            //Guid oid = Guid.Parse(this.Request.QueryString["oid"]);
            //var objectSpace = WebApplication.Instance.CreateObjectSpace(typeof(AttachmentFiles));
            //var attach = objectSpace.GetObjectByKey<AttachmentFiles>(oid);
            //RichEditor.Open(attach.File.FileName, DocumentFormat.Doc, () => attach.File.Content);


            if (IsPostBack == false && string.IsNullOrWhiteSpace(this.Request.QueryString["oid"]) == false)
            {
                Guid oid = Guid.Parse(this.Request.QueryString["oid"]);
                var objectSpace = WebApplication.Instance.CreateObjectSpace(typeof(AttachmentFiles));
                var attach = objectSpace.GetObjectByKey<AttachmentFiles>(oid);
                RichEditor.Open(attach.File.FileName, DocumentFormat.Doc, () => attach.File.Content);
            }
        }

        protected void RichEditor_Saving(object source, DevExpress.Web.Office.DocumentSavingEventArgs e)
        {
            Guid oid = Guid.Parse(this.Request.QueryString["oid"]);
            var objectSpace = WebApplication.Instance.CreateObjectSpace(typeof(AttachmentFiles));
            var attach = objectSpace.GetObjectByKey<AttachmentFiles>(oid);

            attach.File.Content = RichEditor.SaveCopy(DocumentFormat.Doc);
            objectSpace.CommitChanges();

            e.Handled = true;
        }
    }
}